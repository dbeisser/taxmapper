# Change Log

## Jul 12 2017
- The first stable release (1.0.0) of TaxMapper.

## Aug 28 2017
- Version 1.0.1: 
    - bug fix in renaming of temp file for single read.
    - extension of shorten_output to fasta files. Now search for ' vs ' and '\tNO HIT' in alignment files

## Aug 31 2017
- Version 1.0.2:
    - fixed bug in submodules call
