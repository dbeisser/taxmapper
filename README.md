# TaxMapper

**TaxMapper** is an analysis tool for reliable taxonomic mapping of microeukaryotic organisms, forming part of a comprehensive Snakemake workflow for metatranscriptome analysis. It provides taxonomic and functional annotations for NGS reads, facilitating environmental data integration and advanced statistical analysis.

## Quick Start: Recommended Installation

The easiest way to get started with TaxMapper is by using `taxmapper_supplement`, which installs and organizes all necessary tools, environments, and dependencies in one go.

1. Clone `taxmapper_supplement`:
   ```shell
   git clone https://bitbucket.org/dbeisser/taxmapper_supplement.git
   ```
   
2. Navigate to the `taxmapper_supplement/snakemake` folder
3. Modify the config_template.yaml and rename it to config.yaml
4. Run the pipeline by executing:
   ```shell
   ./run.sh
   ```

This script will download all required databases, index them, and execute TaxMapper on a test dataset, ensuring you have everything configured correctly.

By using `taxmapper_supplement`, you avoid the hassle of manual dependency management and ensure a smooth setup process for the full workflow.

## Workflow Overview

TaxMapper provides taxonomic assignments and a variety of output formats, including:

1. Taxonomic assignment matrices and visualizations
2. Functional annotations (KEGG Orthology, pathways, etc.)
3. Quality assessment reports (e.g., FASTQC)
4. Multivariate analyses like RDA and PCA
5. Differential gene expression analysis and pathway enrichment results

## Advanced Installation

If you prefer to set up TaxMapper manually or integrate it into your existing pipeline, follow these steps:

1. Install Miniconda if you haven't already: [Miniconda Installation Guide](https://docs.anaconda.com/miniconda/).
2. Install TaxMapper and its primary dependency, RAPSearch, using:
   ```shell
   conda create -c conda-forge -c bioconda -n taxmapper taxmapper rapsearch=2.24
   conda activate taxmapper
   ```

Alternatively, download the source code:
```shell
git clone https://bitbucket.org/dbeisser/taxmapper
cd taxmapper
python setup.py install
```

## Running TaxMapper

After installation, you can run TaxMapper using the following example command:
```shell
mkdir -p databases/taxonomy
taxmapper run -d databases/taxonomy/meta_database.db -m 100 -f fastq -t 4
```

## System Requirements

TaxMapper requires several tools and packages. If you use `taxmapper_supplement`, these will be automatically managed.

### Python Packages
- `numpy`, `pandas`, `matplotlib`, `deepdish`

### Conda Packages
- `rapsearch`
- Additional (for full workflow): `cutadapt`, `snakemake`, `fastqc`, `cairo`

### R Packages
- `r-base`, `r-data.table`, `bioconductor-edgeR`, `r-vegan`, `r-ggplot2`, `rpy2`, `r-reshape2`, `r-gridextra`, `bioconductor-gage`, `bioconductor-pathview`

## Troubleshooting

1. If you encounter R library path issues, ensure the Conda R library path is prioritized. You can set the correct path in `~/miniconda3/envs/taxmapper/etc/conda/activate.d/env_vars.sh`:

```shell
#!/bin/sh
export R_LIBS=~/miniconda3/envs/taxmapper/lib/R/library
```
2. If forward and reverse read overlap, can they be merged for the taxonomic assignment?
    * It has to be mentioned, that In addition, the alignment step is quite time-consuming and error-prone for short overlaps. A possible solution would be to include an additional rule in the workflow and use a tool such as PANDAseq (https://github.com/neufeld/pandaseq) to align the reads and then provide the assembled sequences as FASTA file to TaxMapper:

```python
rule merge_pairs:
    input: fwd = "sample_R1.fastq",
        rev = "sample_R2.fastq"
    output: "sample_combined.fasta"
    shell: "pandaseq -f {input.fwd} -r {input.rev} −w {output}"
    
```

## How to Cite

Beisser D, Graupner N, Grossmann L, Timm H, Boenigk J, Rahmann S. TaxMapper: an analysis tool, reference database, and workflow for metatranscriptome analysis of eukaryotic microorganisms. BMC Genomics. 2017 Oct 16;18(1):787. [doi:10.1186/s12864-017-4168-6](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-017-4168-6).

