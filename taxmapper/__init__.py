#!/usr/bin/env python3
"""TaxMapper is an analysis tool for a reliable mapping to a provided microeukaryotic reference database.

This module is the main module tasked with launching the desired subcommand
of TaxMapper using the user specified parameters, like the number of threads etc.
"""
import argparse
import glob
import re
import sys

from functools import partial
from multiprocessing import Pool
from os import path, makedirs
from subprocess import call

from .map_tax import run
from .filter_tax import run
from .count_tax import run
from .plot_tax import run
from .search_tax import run
from .initialize import run
from .version import __version__


def main():
    """Select and run module based on the user input."""
    parser = get_argument_parser()
    args = parser.parse_args()
    if args.version:
        print(__version__)
        exit()
    if args.subcommands == None:
        parser.print_help()
        exit()
    if args.subcommands == 'search':
        search_tax.run(args)
    if args.subcommands == 'map':
        map_tax.run(parser, args)
    if args.subcommands == 'filter':
        filter_tax.run(parser, args)
    if args.subcommands == 'count':
        count_tax.run(parser, args)
    if args.subcommands == 'plot':
        plot_tax.run(parser, args)
    if args.subcommands == 'run':
        run_complete(parser, args)
    if args.subcommands == 'initialize':
        initialize.run(args)


def get_argument_parser():
    """Construct and return an argument parser including all subcommands."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--version",
        dest="version",
        action='store_true',
    )
    # define list of arguments shown in help
    subparsers = parser.add_subparsers(dest='subcommands', metavar='{search,map,filter,count,plot,run}')
    subparsers.required = False
    initialize_parser = subparsers.add_parser('initialize', add_help=False)
    search_parser = subparsers.add_parser('search', help="RAPSearch wrapper to search with reads in database")
    map_parser = subparsers.add_parser('map', help="Map RAPSearch results to taxonomic groups")
    filter_parser = subparsers.add_parser('filter', help="Filter taxonomic assignments")
    count_parser = subparsers.add_parser('count', help="Count taxonomic groups")
    plot_parser = subparsers.add_parser('plot', help="Nomalize and plot taxonomic groups")
    map_tax.sub(map_parser)
    filter_tax.sub(filter_parser)
    count_tax.sub(count_parser)
    plot_tax.sub(plot_parser)
    search_tax.sub(search_parser)
    initialize.sub(initialize_parser)

    complete_parser = subparsers.add_parser('run', help="Run complete pipeline")
    complete_parser.add_argument(
        '-d', '--database',
        dest='database',
        type=str,
        help='Database path for RAPseach database index',
        required=True,
    )
    complete_parser.add_argument(
        '-f', '--folder',
        dest='folder',
        type=str,
        help='Folder with reads in fasta or fastq format',
        required=True,
    )
    complete_parser.add_argument(
        '-r', '--reverse',
        dest='reverse',
        type=str,
        default="True",
        help='Reads also contain reverse read, [default: True]',
    )
    complete_parser.add_argument(
        '-s', '--suffix',
        dest='suffix',
        type=str,
        default="_R1,_R2",
        help='Suffix of paired end reads, [default: "_R1,_R2"]',
    )
    complete_parser.add_argument(
        '-m',
        dest='length',
        type=int,
        help='Maximum read length',
        required=True,
    )
    complete_parser.add_argument(
        '-o', '--out',
        dest='output',
        type=str,
        default="results",
        help='Output folder, [default: "results"]',
    )
    complete_parser.add_argument(
        '-t', '--threads',
        dest='threads',
        default=4,
        type=int,
        help='Number of threads, [default: 4]',
    )
    return parser

def run_complete(parser, args):
    """Perform the whole TaxMapper pipeline.

    Arguments:
        parser (ArgumentParser): Used to relay errors to the user.
        args (argparse.Namespace): User parameters.
    """
    if not (args.folder):
        parser.error("You have to specify a folder with the reads!")
    folder = args.folder
    if not path.exists(args.output):
        makedirs(args.output)
    programPath = path.dirname(path.realpath(__file__))
    # create db index
    # get path of own python executable and use it to call initilize
    python = sys.executable
    tool = "taxmapper initialize"
    command = [tool+" -d {0}".format(args.database)]
    call(command, shell=True)
    # compute taxonomy mapping for paired or single reads
    # paired reads
    if(args.reverse):
        # find input files
        fwd, rev = args.suffix.split(",")
        ffiles = glob.glob(path.join(folder, "*"+fwd+"*"))
        rfiles = glob.glob(path.join(folder, "*"+rev+"*"))
        if(len(ffiles) == 0 or len(rfiles) == 0):
            parser.error("The read folder is empty or does not contain paired reads")
        outbase = [path.join(args.output, path.basename(re.sub(fwd+".*", "", ffile))) for ffile in ffiles]
        outfiles = [basename+"_hits" for basename in outbase]
        ################################################################################
        # search
        # number threads >= 10 and more than one sample
        if(args.threads >= 10 and len(ffiles) > 1):
            tool = "taxmapper search"
            pool = args.threads // 10 # number of subprocesses
            commands = [tool+" -f {0} -r {1} -o {2} -t 10 -d {3}".format(ffiles[x], rfiles[x], outfile, args.database) for x, outfile in enumerate(outfiles)]
            with Pool(processes=pool) as p:
                p.map(partial(call, shell=True), commands)
        # number threads < 10
        else:
            tool = "taxmapper search"
            pool = 1
            commands = [tool+" -f {0} -r {1} -o {2} -t {3} -d {4}".format(ffiles[x], rfiles[x], outfile, args.threads, args.database) for x, outfile in enumerate(outfiles)]
            with Pool(processes=pool) as p:
                p.map(partial(call, shell=True), commands)
        ################################################################################
        # taxonomy mapping
        tool = "taxmapper map"
        faln = [basename+"_hits_1.aln" for basename in outbase]
        raln = [basename+"_hits_2.aln" for basename in outbase]
        taxa_outfiles = [basename+"_taxa.tsv" for basename in outbase]
        commands =  [tool+" -m {0} -f {1} -r {2} -o {3} -t 2 -m {4}".format(args.length, faln[x], raln[x], taxa_outfile, args.length) for x, taxa_outfile in enumerate(taxa_outfiles)]
        pool = args.threads // 2
        with Pool(processes=pool) as p:
            p.map(partial(call, shell=True), commands)

    # unpaired reads
    else:
        # find input files
        files = glob.glob(path.join(folder, "*"))
        if(len(files)==0):
            parser.error("The read folder is empty")
        outbase = [path.join(folder, path.basename(re.sub(fwd+".*", "", f))) for f in files]
        outfiles = [basename+"_hits" for basename in outbase]
        ################################################################################
        # search
        # number threads >= 10 and more than 1 sample
        if(args.threads >= 10 and len(files) > 1):
            tool = "taxmapper search"
            pool = args.threads // 10 # number of subprocesses
            commands = [tool+" -f {0} -o {1} -t 10 -d {2}".format(files[x], outfile, args.database) for x, outfile in enumerate(outfiles)]
            with Pool(processes=pool) as p:
                p.map(partial(call, shell=True), commands)
        # number threads < 10
        else:
            tool = "taxmapper search"
            pool = 1
            command = [tool+" -f {0} -o {1} -t {2} -d {}".format(files[x], outfile, args.threads, args.database) for x, outfile in enumerate(outfiles)]
            with Pool(processes=1) as p:
                p.map(partial(call, shell=True), commands)
        ################################################################################
        # taxonomy mapping
        tool = "taxmapper map"
        faln = [basename+"_hits_1.aln" for basename in outbase]
        taxa_outfiles = [basename+"_taxa.tsv" for basename in outbase]
        commands = [tool+" -m {0} -f {1} -o {2} -t 2 -m {3}".format(args.length, faln[x], taxa_outfile, args.length) for x, taxa_outfile in enumerate(taxa_outfiles)]
        pool = args.threads
        with Pool(processes=pool) as p:
            p.map(partial(call, shell=True), commands)
    ################################################################################
    # filter
    tool = "taxmapper filter"
    tax = taxa_outfiles
    filtered_outfiles = [basename+"_taxa_filtered.tsv" for basename in outbase]
    commands = [tool+" -i {0} -o {1}".format(tax_raw, tax_filtered) for tax_raw, tax_filtered in zip(tax, filtered_outfiles)]
    pool = args.threads
    with Pool(processes=pool) as p:
        p.map(partial(call, shell=True), commands)
    ###############################################################################
    # count
    tool = "taxmapper count"
    out1 = [basename+"_taxa_counts_level1.tsv" for basename in outbase]
    out2 = [basename+"_taxa_counts_level2.tsv" for basename in outbase]
    commands = [tool+" -i {0} --out1 {1} --out2 {2}".format(filtered_outfiles[x], out1[x], out2[x]) for x, _ in enumerate(filtered_outfiles)]
    pool = args.threads
    with Pool(processes=pool) as p:
        p.map(partial(call, shell=True), commands)
    ################################################################################
    # plot
    tax1 = out1
    tax2 = out2
    outfolder = path.dirname(tax1[0])
    out1 = path.join(outfolder, "taxa_freq_norm_level1.svg")
    out2 = path.join(outfolder, "taxa_freq_norm_level2.svg")
    out3 = path.join(outfolder, "taxa_freq_norm_level1.tsv")
    out4 = path.join(outfolder, "taxa_freq_norm_level2.tsv")
    out5 = path.join(outfolder, "taxa_counts_level1.tsv")
    out6 = path.join(outfolder, "taxa_counts_level2.tsv")
    samples = [path.basename(x) for x in outbase]
    tool = "taxmapper plot"
    commands = [
        tool+" -t {0} -p {1} -s {2} -f {3} -c {4}".format(" ".join(tax1), out1, " ".join(samples), out3, out5),
        tool+" -t {0} -p {1} -s {2} -f {3} -c {4}".format(" ".join(tax2), out2, " ".join(samples), out4, out6),
    ]
    pool = 2
    with Pool(processes=pool) as p:
        p.map(partial(call, shell=True), commands)


if __name__ == '__main__':
    main()
