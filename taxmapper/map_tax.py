#!/usr/bin/env python3
"""Postprocess .aln files returned by RAPsearch, analysing the number of hits and target taxa.
Write results into a taxa.tsv file.
"""
import argparse
import collections
import re

from functools import partial
from math import floor
from multiprocessing import Pool
from os import path, rename, remove

# dictionary taxonomy
# TODO: Move this to an own file and import it from there
TAX_DICT1 = {"MT1":"Alveolata", "MT2":"Alveolata", "MT3":"Alveolata", "MT4":"Alveolata", "MT5":"Alveolata", "MT6":"Alveolata", "MT7":"Alveolata", "MT8":"Alveolata", "MT9":"Alveolata", "MT10":"Alveolata", "MT11":"Alveolata", "MT12":"Alveolata", "MT13":"Alveolata", "MT14":"Alveolata", "MT15":"Alveolata", "MT16":"Alveolata", "MT17":"Alveolata", "MT18":"Alveolata", "MT19":"Alveolata", "MT20":"Alveolata", "MT21":"Alveolata", "MT22":"Alveolata", "MT23":"Alveolata", "MT25":"Alveolata", "MT26":"Alveolata", "MT28":"Alveolata", "MT29":"Archaeplastida", "MT30":"Archaeplastida", "MT31":"Archaeplastida", "MT32":"Archaeplastida", "MT33":"Archaeplastida", "MT34":"Archaeplastida", "MT35":"Archaeplastida", "MT36":"Archaeplastida", "MT37":"Archaeplastida", "MT38":"Archaeplastida", "MT39":"Archaeplastida", "MT40":"Archaeplastida", "MT41":"Archaeplastida", "MT42":"Archaeplastida", "MT43":"Archaeplastida", "MT44":"Archaeplastida", "MT45":"Archaeplastida", "MT46":"Archaeplastida", "MT47":"Archaeplastida", "MT48":"Archaeplastida", "MT49":"Archaeplastida", "MT50":"Archaeplastida", "MT51":"Excavata", "MT52":"Excavata", "MT53":"Excavata", "MT54":"Excavata", "MT55":"Excavata", "MT56":"Excavata", "MT57":"Excavata", "MT58":"Excavata", "MT59":"Excavata", "MT60":"Hacrobia", "MT61":"Hacrobia", "MT62":"Hacrobia", "MT63":"Hacrobia", "MT64":"Hacrobia", "MT65":"Hacrobia", "MT66":"Hacrobia", "MT67":"Hacrobia", "MT68":"Hacrobia", "MT69":"Hacrobia", "MT70":"Hacrobia", "MT71":"Rhizaria", "MT72":"Rhizaria", "MT74":"Rhizaria", "MT75":"Rhizaria", "MT76":"Rhizaria", "MT77":"Rhizaria", "MT78":"Rhizaria", "MT79":"Stramenopile", "MT80":"Stramenopile", "MT81":"Stramenopile", "MT82":"Stramenopile", "MT83":"Stramenopile", "MT84":"Stramenopile", "MT85":"Stramenopile", "MT86":"Stramenopile", "MT87":"Stramenopile", "MT88":"Stramenopile", "MT89":"Stramenopile", "MT90":"Stramenopile", "MT91":"Stramenopile", "MT92":"Stramenopile", "MT93":"Stramenopile", "MT94":"Stramenopile", "MT95":"Stramenopile", "MT96":"Stramenopile", "MT97":"Stramenopile", "MT98":"Stramenopile", "MT99":"Stramenopile", "MT100":"Stramenopile", "MT101":"Stramenopile", "MT102":"Stramenopile", "MT103":"Stramenopile", "MT104":"Stramenopile", "MT105":"Stramenopile", "MT106":"Stramenopile", "MT107":"Stramenopile", "MT108":"Stramenopile", "MT109":"Stramenopile", "MT110":"Stramenopile", "MT111":"Stramenopile", "MT112":"Stramenopile", "MT113":"Stramenopile", "MT114":"Stramenopile", "MT115":"Stramenopile", "MT116":"Stramenopile", "MT117":"Stramenopile", "MT118":"Stramenopile", "MT119":"Amorphea", "MT120":"Amorphea", "MT121":"Amorphea", "MT122":"Amorphea", "MT123":"Amorphea", "MT124":"Amorphea", "MT125":"Amorphea", "MT126":"Amorphea", "MT127":"Amorphea", "MT128":"Amorphea", "MT129":"Amorphea", "MT130":"Amorphea", "MT131":"Amorphea", "MT132":"Amorphea", "MT133":"Amorphea", "MT134":"Amorphea", "MT136":"Amorphea", "MT137":"Amorphea", "MT138":"Amorphea", "MT139":"Amorphea", "MT140":"Amorphea", "MT141":"Amorphea", "MT142":"Amorphea", "MT143":"Amorphea", "MT144":"Amorphea", "MT145":"Amorphea", "MT146":"Amorphea"}
TAX_DICT2 = {"MT1":"Apicomplexa", "MT2":"Chromerida", "MT3":"Ciliophora", "MT4":"Ciliophora", "MT5":"Dinophyceae", "MT6":"Dinophyceae", "MT7":"Perkinsea", "MT8":"Apicomplexa", "MT9":"Apicomplexa", "MT10":"Apicomplexa", "MT11":"Ciliophora", "MT12":"Ciliophora", "MT13":"Ciliophora", "MT14":"Ciliophora", "MT15":"Ciliophora", "MT16":"Ciliophora", "MT17":"Dinophyceae", "MT18":"Dinophyceae", "MT19":"Dinophyceae", "MT20":"Dinophyceae", "MT21":"Dinophyceae", "MT22":"Dinophyceae", "MT23":"Dinophyceae", "MT25":"Dinophyceae", "MT26":"Dinophyceae", "MT28":"Chromerida", "MT29":"Chlorophyta", "MT30":"Glaucocystophyceae", "MT31":"Rhodophyta", "MT32":"Chlorophyta", "MT33":"Chlorophyta", "MT34":"Streptophyta", "MT35":"Streptophyta", "MT36":"Streptophyta", "MT37":"Streptophyta", "MT38":"Glaucocystophyceae", "MT39":"Rhodophyta", "MT40":"Rhodophyta", "MT41":"Chlorophyta", "MT42":"Chlorophyta", "MT43":"Chlorophyta", "MT44":"Chlorophyta", "MT45":"Chlorophyta", "MT46":"Chlorophyta", "MT47":"Chlorophyta", "MT48":"Chlorophyta", "MT49":"Chlorophyta", "MT50":"Streptophyta", "MT51":"Euglenozoa", "MT52":"Euglenozoa", "MT53":"Euglenozoa", "MT54":"Fornicata", "MT55":"Heterolobosea", "MT56":"Parabasalia", "MT57":"Euglenozoa", "MT58":"Fornicata", "MT59":"Heterolobosea", "MT60":"Cryptophyta", "MT61":"Cryptophyta", "MT62":"Cryptophyta", "MT63":"Cryptophyta", "MT64":"Haptophyta", "MT65":"Haptophyta", "MT66":"Haptophyta", "MT67":"Haptophyta", "MT68":"Haptophyta", "MT69":"Haptophyta", "MT70":"Haptophyta", "MT71":"Cercozoa", "MT72":"Cercozoa", "MT74":"Foraminifera", "MT75":"Foraminifera", "MT76":"Foraminifera", "MT77":"Cercozoa", "MT78":"Foraminifera", "MT79":"Bacillariophyta", "MT80":"Bigyra", "MT81":"Stramenopile Rest", "MT82":"Bigyra", "MT83":"Stramenopile Rest", "MT84":"Stramenopile Rest", "MT85":"Stramenopile Rest", "MT86":"Stramenopile Rest", "MT87":"Chrysophyceae", "MT88":"Chrysophyceae", "MT89":"Bigyra", "MT90":"Pseudofungi", "MT91":"Pseudofungi", "MT92":"Stramenopile Rest", "MT93":"Bigyra", "MT94":"Pseudofungi", "MT95":"Stramenopile Rest", "MT96":"Chrysophyceae", "MT97":"Chrysophyceae", "MT98":"Chrysophyceae", "MT99":"Chrysophyceae", "MT100":"Stramenopile Rest", "MT101":"Stramenopile Rest", "MT102":"Stramenopile Rest", "MT103":"Stramenopile Rest", "MT104":"Bacillariophyta", "MT105":"Bacillariophyta", "MT106":"Bacillariophyta", "MT107":"Bacillariophyta", "MT108":"Bacillariophyta", "MT109":"Bacillariophyta", "MT110":"Bacillariophyta", "MT111":"Bacillariophyta", "MT112":"Bacillariophyta", "MT113":"Bacillariophyta", "MT114":"Bacillariophyta", "MT115":"Bacillariophyta", "MT116":"Bacillariophyta", "MT117":"Bacillariophyta", "MT118":"Stramenopile Rest", "MT119":"Amoebozoa", "MT120":"Amoebozoa", "MT121":"Amoebozoa", "MT122":"Apusozoa", "MT123":"Choanoflagellida", "MT124":"Fungi", "MT125":"Fungi", "MT126":"Fungi", "MT127":"Fungi", "MT128":"Fungi", "MT129":"Fungi", "MT130":"Metazoa", "MT131":"Opisthokonta Rest", "MT132":"Opisthokonta Rest", "MT133":"Amoebozoa", "MT134":"Amoebozoa", "MT136":"Amoebozoa", "MT137":"Amoebozoa", "MT138":"Choanoflagellida", "MT139":"Metazoa", "MT140":"Metazoa", "MT141":"Metazoa", "MT142":"Metazoa", "MT143":"Metazoa", "MT144":"Metazoa", "MT145":"Metazoa", "MT146":"Metazoa"}
TAX_DICT_COMPLETE = {"MT1":"Alveolata; Apicomplexa; Aconoidasida; Plasmodium falciparum 3D7", "MT2":"Alveolata; Chromerida; Chromera; Chromera velia", "MT3":"Alveolata; Ciliophora; Intramacronucleata; Euplotes focardii", "MT4":"Alveolata; Ciliophora; Intramacronucleata; Paramecium tetraurelia", "MT5":"Alveolata; Dinophyceae; Gonyaulacales; Alexandrium tamarense", "MT6":"Alveolata; Dinophyceae; Gymnodiniales; Karena brevis", "MT7":"Alveolata; Perkinsea; Perkinsus marinus", "MT8":"Alveolata; Apicomplexa; Conoidasida; Lankesteria abbottii", "MT9":"Alveolata; Apicomplexa; Conoidasida; Neospora caninum", "MT10":"Alveolata; Apicomplexa; Aconoidasida; Babesia microti", "MT11":"Alveolata; Ciliophora; Intramacronucleata; Pseudocohnilembus persalinus", "MT12":"Alveolata; Ciliophora; Intramacronucleata; Platyophrya macrostoma", "MT13":"Alveolata; Ciliophora; Intramacronucleata; Strombidium rassoulzadegani", "MT14":"Alveolata; Ciliophora; Intramacronucleata; Mesodinium pulex", "MT15":"Alveolata; Ciliophora; Intramacronucleata; Tiarina fusus", "MT16":"Alveolata; Ciliophora; Postciliodesmatophora; Blepharisma japonicum", "MT17":"Alveolata; Dinophyceae; Dinophysiales; Dinophysis acuminata", "MT18":"Alveolata; Dinophyceae; Gonyaulacales; Ceratium fusus", "MT19":"Alveolata; Dinophyceae; Gonyaulacales; Crypthecodinium cohnii", "MT20":"Alveolata; Dinophyceae; Peridiniales; Brandtodinium nutriculum", "MT21":"Alveolata; Dinophyceae; Peridiniales; Heterocapsa arctica", "MT22":"Alveolata; Dinophyceae; Suessiales; Polarella glacialis", "MT23":"Alveolata; Dinophyceae; Oxyrrhinales; Oxyrrhis marina", "MT25":"Alveolata; Dinophyceae; Gymnodiniales; Gymnodinium catenatum", "MT26":"Alveolata; Dinophyceae; Noctilucales; Noctiluca scintillans", "MT28":"Alveolata; Chromerida; Vitrella; Vitrella brassicaformis", "MT29":"Archaeplastida; Chlorophyta; Prasinophyceae; Pyramimonas parkeae", "MT30":"Archaeplastida; Glaucocystophyceae; Cyanoptyche gloeocystis", "MT31":"Archaeplastida; Rhodophyta; Rhodellophyceae; Rhodella maculata", "MT32":"Archaeplastida; Chlorophyta; Chlorophyceae; Chlamydomonas reinhardtii", "MT33":"Archaeplastida; Chlorophyta; Trebouxiphyceae; Chlorella variabilis", "MT34":"Archaeplastida; Streptophyta; Embryophyta; Bryophyta; Physcomitrella patens", "MT35":"Archaeplastida; Streptophyta; Embryophyta; Tracheophyta; Genlisea aurea", "MT36":"Archaeplastida; Streptophyta; Embryophyta; Tracheophyta; Populus trichocarpa", "MT37":"Archaeplastida; Streptophyta; Embryophyta; Tracheophyta; Selaginella moellendorffii", "MT38":"Archaeplastida; Glaucocystophyceae; Gloeochaete witrockiana", "MT39":"Archaeplastida; Rhodophyta; Bangiophyceae; Timspurckia oligopyrenoides", "MT40":"Archaeplastida; Rhodophyta; Compsopogonophyceae; Compsopogon coeruleus", "MT41":"Archaeplastida; Chlorophyta; Chlorodendrophyceae; Tetraselmis astigmatica", "MT42":"Archaeplastida; Chlorophyta; Chlorophyceae; Dunaliella tertiolecta", "MT43":"Archaeplastida; Chlorophyta; Chlorophyceae; Monoraphidium neglectum", "MT44":"Archaeplastida; Chlorophyta; Prasinophyceae; Picocystis salinarum", "MT45":"Archaeplastida; Chlorophyta; Prasinophyceae; Pycnococcus provasolii", "MT46":"Archaeplastida; Chlorophyta; Prasinophyceae; Nephroselmis pyriformis", "MT47":"Archaeplastida; Chlorophyta; Prasinophyceae; Prasinoderma coloniale", "MT48":"Archaeplastida; Chlorophyta; Prasinophyceae; Micromonas pusilla", "MT49":"Archaeplastida; Chlorophyta; Trebouxiphyceae; Coccomyxa subellipsoidea", "MT50":"Archaeplastida; Streptophyta; Klebsormidium flaccidium", "MT51":"Excavata; Euglenozoa; Euglenida; Eutreptiella gymnastica-like", "MT52":"Excavata; Euglenozoa; Kinetoplastida; Leishmania donovani", "MT53":"Excavata; Euglenozoa; Kinetoplastida; Trypanosoma brucei gambiense DAL972", "MT54":"Excavata; Fornicata; Diplomonadida; Giardia lamblia ATCC 50803", "MT55":"Excavata; Heterolobosea; Naegleria gruberi", "MT56":"Excavata; Parabasalia; Trichomonadida; Trichomonas vaginalis G3", "MT57":"Excavata; Euglenozoa; Kinetoplastida; Neobodo designis", "MT58":"Excavata; Fornicata; Diplomonadida; Spironucleus salmonicida", "MT59":"Excavata; Heterolobosea; Percolomonas cosmopolitus", "MT60":"Hacrobia; Cryptophyta; Cryptomonadales; Guillardia theta", "MT61":"Hacrobia; Cryptophyta; Cryptomonadales; Goniomonas pacifica", "MT62":"Hacrobia; Cryptophyta; Chryptomonadales; Rhodomonas abbreviata", "MT63":"Hacrobia; Cryptophyta; Cryptomonadales; Cryptomonas curvata", "MT64":"Hacrobia; Haptophyta; Prymnesiophyceae; Isochrysidales; Emiliania huxleyi CCMP1516", "MT65":"Hacrobia; Haptophyta; Prymnesiophyceae; Prymnesiales; Prymnesium parvum", "MT66":"Hacrobia; Haptophyta; Prymnesiophyceae; Prymnesiales; Chrysochromulina polylepis", "MT67":"Hacrobia; Haptophyta; Pavlovaceae; Pavlovales; Pavlova sp", "MT68":"Hacrobia; Haptophyta; Prymnesiophyceae; Isochrysidales; Isochrysis galbana", "MT69":"Hacrobia; Haptophyta; Prymnesiophyceae; Coccolithales; Pleurochrysis carterae", "MT70":"Hacrobia; Haptophyta; Prymnesiophyceae; Phaeocystales; Phaeocystis cordata", "MT71":"Rhizaria; Cercozoa; Filosa; Reticulofilosa; Chlorarachniophyta; Lotharella globosa", "MT72":"Rhizaria; Cercozoa; Filosa; Reticulofilosa; Chlorarachniophyta; Chlorarachnion reptans", "MT74":"Rhizaria; Retaria; Foraminifera; Rotaliida; Elphidium margaritaceum", "MT75":"Rhizaria; Retaria; Foraminifera; Monothalamids; Reticulomyxa filosa", "MT76":"Rhizaria; Retaria; Foraminafera; Miliolida; Sorites sp", "MT77":"Rhizaria; Cercozoa; Endomyxa; Plasmodiophorida; Plasmodiophora brassicae", "MT78":"Rhizaria; Retaria; Foraminifera; Rotaliida; Ammonia sp", "MT79":"Stramenopile; Ochrophyta; Bacillariophyta; Fragilariophyceae; Asterionellopsis glacialis", "MT80":"Stramenopile; Bigyra; Bicosoecida; Cafeteria roenbergensis", "MT81":"Stramenopile; Ochrophyta; Eustigmatophyceae; Nannochloropsis gaditana", "MT82":"Stramenopile; Bigyra; Labyrinthista; Schizochytrium aggregatum", "MT83":"Stramenopile; Ochrophyta; Dictyochophyceae; Pteridomonas danica", "MT84":"Stramenopile; Ochrophyta; Pelagophyceae; Pelagococcus subviridis", "MT85":"Stramenopile; Ochrophyta; Raphidophyceae; Heterosigma akashiwo", "MT86":"Stramenopile; Ochrophyta; Xanthophyceae; Vaucheria litorea", "MT87":"Stramenopile; Ochrophyta; Chrysophyceae; Chromulina nebulosa", "MT88":"Stramenopile; Ochrophyta; Chrysophyceae; Mallomonas sp", "MT89":"Stramenopile; Bigyra; Blastocystis; Blastocystis hominis", "MT90":"Stramenopile; Pseudofungi; Peronosporales; Phytophthora sojae", "MT91":"Stramenopile; Pseudofungi; Saprolegniales; Aphanomyces astaci", "MT92":"Stramenopile; Ochrophyta; Phaeophyceae; Ectocarpus siliculosus", "MT93":"Stramenopile; Bigyra; Labyrinthista; Aplanochytrium stocchinoi", "MT94":"Stramenopile; Pseudofungi; Albuginales; Albugo candida", "MT95":"Stramenopile; Ochrophyta; Pinguiophyceae; Phaeomonas parva", "MT96":"Stramenopile; Ochrophyta; Chrysophyceae; Paraphysomonas imperforata", "MT97":"Stramenopile; Ochrophyta; Chrysophyceae; Poterioochromonas malhamensis", "MT98":"Stramenopile; Ochrophyta; Chrysophyceae; Pedospumella encystans", "MT99":"Stramenopile; Ochrophyta; Chrysophyceae; Dinobryon sp", "MT100":"Stramenopile; Ochrophyta; Dictyochophyceae; Dictyocha speculum", "MT101":"Stramenopile; Ochrophyta; Pelagophyceae; Sarcinochrysis sp", "MT102":"Stramenopile; Ochrophyta; Pelagophyceae; Chrysoreinhardia sp", "MT103":"Stramenopile; Ochrophyta; Pelagophyceae; Aureococcus anophagefferens", "MT104":"Stramenopile; Ochrophyta; Bacillariophyta; Bacillariophyceae; Nitschia punctata", "MT105":"Stramenopile; Ochrophyta; Bacillariophyta; Bacillariophyceae; Amphora coffeaeformis", "MT106":"Stramenopile; Ochrophyta; Bacillariophyta; Bacillariophyceae; Stauroneis constricta", "MT107":"Stramenopile; Ochrophyta; Bacillariophyta; Coscinodiscophyceae; Chaetoceros debilis", "MT108":"Stramenopile; Ochrophyta; Bacillariophyta; Coscinodiscophyceae; Corethron pennatum", "MT109":"Stramenopile; Ochrophyta; Bacillariophyta; Coscinodiscophyceae; Stephanopyxis turris", "MT110":"Stramenopile; Ochrophyta; Bacillariophyta; Coscinodiscophyceae; Cosconidiscus wailesii", "MT111":"Stramenopile; Ochrophyta; Bacillariophyta; Coscinodiscophyceae; Proboscia inermis", "MT112":"Stramenopile; Ochrophyta; Bacillariophyta; Coscinodiscophyceae; Thalassiosira gravida", "MT113":"Stramenopile; Ochrophyta; Bacillariophyta; Fragilariophyceae; Striatella unipunctata", "MT114":"Stramenopile; Ochrophyta; Bacillariophyta; Fragilariophyceae; Cyclophora tenuis", "MT115":"Stramenopile; Ochrophyta; Bacillariophyta; Mediophyceae; Triceratium dubium", "MT116":"Stramenopile; Ochrophyta; Bacillariophyta; Mediophyceae; Minutocellus polymorphus", "MT117":"Stramenopile; Ochrophyta; Bacillariophyta; Mediophyceae; Helicotheca tamensis", "MT118":"Stramenopile; Ochrophyta; Bolidophyceae; Bolidomonas sp", "MT119":"Amorphea; Amoebozoa; Conosa; Archamoeba; Entamoeba dispar", "MT120":"Amorphea; Amoebozoa; Lobosea; Discosea; Acanthamoeba castellanii", "MT121":"Amorphea; Amoebozoa; Conosa; Mycetozoa; Dictyostelium fasciculatum", "MT122":"Amorphea; Apusozoa; Apusomonads; Amastigomonas sp", "MT123":"Amorphea; Opisthokonta; Choanoflagellida; Salpingoecidae; Monosiga brevicollis", "MT124":"Amorphea; Opisthokonta; Fungi; Chytridiomycota; Batrachochytrium dendrobatidis", "MT125":"Amorphea; Opisthokonta; Fungi; Cryptomycota; Rozella allomycis", "MT126":"Amorphea; Opisthokonta; Fungi; Dikarya; Ascomycota; Saccharomyces cerevisiae", "MT127":"Amorphea; Opisthokonta; Fungi; Dikarya; Basidiomycota; Cryptococcus gattii", "MT128":"Amorphea; Opisthokonta; Fungi; Glomeromycota; Rhizophagus irregularis", "MT129":"Amorphea; Opisthokonta; Fungi; Microsporidia; Encephalitozoon intestinalis", "MT130":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Arthropoda; Daphnia pulex", "MT131":"Amorphea; Opisthokonta; Fonticula; Fonticula alba", "MT132":"Amorphea; Opisthokonta; Ichthyosporea; Capsaspora owczarzaki", "MT133":"Amorphea; Amoebozoa; Lobosea; Discosea; Pessonella sp", "MT134":"Amorphea; Amoebozoa; Lobosea; Discosea; Mayorella sp", "MT136":"Amorphea; Amoebozoa; Lobosea; Stereomyxa; Stereomyxa ramosa", "MT137":"Amorphea; Amoebozoa; Lobosea; Tubulinea; Sexangularia sp.", "MT138":"Amorphea; Opisthokonta; Choanoflagellida; Salpingoecidae; Salpingoeca rosetta", "MT139":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Nematodes; Caenorhabditis elegans", "MT140":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Nematodes; Trichinella murrelli", "MT141":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Arthropods; Metaseiulus occidentalis", "MT142":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Arthropods; Stegodyphus mimosarum", "MT143":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Cnidaria; Hydra vulgaris", "MT144":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Annelida; Capitella teleta", "MT145":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Rotifera; Adineta vaga", "MT146":"Amorphea; Opisthokonta; Metazoa; Eumetazoa; Mollusca; Biomphalaria glabrata"}


def sub(parser):
    parser.add_argument(
        '-m',
        dest='length',
        type=int,
        help='Maximum read length',
        required=True,
    )
    parser.add_argument(
        '-f', '--forward',
        dest='file1',
        type=str,
        help='Forward read aln file',
        required=True,
    )
    parser.add_argument(
        '-r', '--reverse',
        dest='file2',
        type=str,
        help='Reverse read aln file [optional]',
    )
    parser.add_argument(
        '-o', '--out',
        dest='output',
        type=str,
        default="taxa.tsv",
        help='Output file [default: taxa.tsv]',
    )
    parser.add_argument(
        '-c', '--combine',
        dest='combine',
        type=str,
        default="best",
        help='How to combine forward and reverse hits, for "concordant" forward and reverse have to map to the same taxon, for "best" the best hit from forward and reverse is returned [default: best]')
    parser.add_argument(
        '-t', '--threads',
        dest='threads',
        default=2,
        type=int,
        help='Number of threads, used to map forward and reverse reads in parallel [default: 2]',
    )


def run(parser, args):
    """Start analysis of .aln file or files."""
    output = path.splitext(args.output)[0]
    if(args.file1 and args.file2):
        # handle call with paired-end data
        aln = (args.file1, args.file2)
        # run forward and reverse in parallel
        with Pool(processes=args.threads) as pool:
            pool.map(partial(analyse_hits, maxlength=args.length, output=output), aln)
        alnfile1 = path.basename(args.file1)
        alnfile2 = path.basename(args.file2)
        tmp1 = path.join(path.split(output)[0], "tmp") + "_" + alnfile1
        tmp2 = path.join(path.split(output)[0], "tmp") + "_" + alnfile2
        combine_hits(tmp1, tmp2, args.combine, args.output)  # deletes the tmp files
    else:
        # handle call with single end data
        analyse_hits(args.file1, args.length, output)
        alnfile1 = path.basename(args.file1)
        tmp1 = path.join(path.split(output)[0], "tmp") + "_" + alnfile1
        rename(tmp1, args.output)
    calculate_mean_identities(args.output)


def analyse_hits(aln, maxlength, output):
    """Return the accepted hit or NA for each read

    Arguments:
        aln (str/path): Path to an alignment file.
        maxlength (int): Maximum number of hits. Supplied by the user with the -m parameter.
        output (str/path): Path for the output files. Supplied by the user with the -o parameter.
    """
    alnfile = path.basename(aln)
    # output = output + "_" + alnfile
    output = path.join(path.split(output)[0], "tmp") + "_" + alnfile
    with open(aln, 'r') as alnfile:
        with open(output, "w") as out:
            # Collect alignment parameters from the first line of the alignment file
            firstline = alnfile.readline().rstrip()
            firstline = re.split("[ =\t]", firstline)
            # query is the readname
            query = firstline[0]
            hits = []
            # test for NO HIT
            if(firstline[1] != "NO"):
                #append query, hit, log e-value, percent identity, alignment length
                hits.append([firstline[i] for i in [0,2,6,8,10]])
                # hits.append([query, hit, log_e_value, percent_identity, aln_length])
            else:
                hits.append([query, "NA"])
            # process the rest of the file
            for line in alnfile:
                line = line.rstrip()
                # test if hit
                if(" vs " in line):
                    line = re.split("[ =\t]", line)
                    if(line[0] == query):
                        hits.append([line[i] for i in [0,2,6,8,10]])
                    else:
                        result = best_result(hits, maxlength)
                        out.write("\t".join(result)+"\n")
                        query = line[0]
                        hits = []
                        hits.append([line[i] for i in [0,2,6,8,10]])
                # else NO HIT
                else:
                    result = best_result(hits, maxlength)
                    out.write("\t".join(result)+"\n")
                    line = re.split("[ =\t]", line)
                    query = line[0]
                    hits = []
                    hits.append([query, "NA"])
            # write the maxlength best hits for this alignment to the output file
            result = best_result(hits, maxlength)
            out.write("\t".join(result)+"\n")


def best_result(hits, maxlength):
    """Get the maxlength best results from a list of hits.

    Arguments:
        hits (list): List of mapping hits. Each mapping list is a split up
            version of one line in a RAPSearch alignment file.
        maxlength (int): Maximum number of hits.
            (Supplied by the user via the -m parameter.)

    Returns:
        list: Results, consisting of the following entries:
        First entry is bestHit[0] (the qeury),
        average identity best hit, average identity of other hit, ratio of both,
        e-value best hit, e-value other hit, difference between e-values,
        taxon 1 best hit, taxon 2 best hit, taxon 3 best hit,
        taxon 1 other hit, taxon 2 other hit, taxon3 other hit.

        Best hit is a string, the identity values and e-values are string
        representations of floats, rounded to two decimals.

        The three values for best hit and other hit are for the identified
        supergroup, group and taxonomic lineage (taxon 1 - 3).
    """
    maxalign = floor(maxlength / 3)
    if(hits[0][1] == 'NA'):
            result = [hits[0][0], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
    else:
        # values are first 1-20 hits from RAPSearch
        # list of lists with values:
        # query, hit, log e-value, percent identity, alignment length
        bestHit = hits[0]
        taxID = bestHit[1].split("_")[0]
        bestTax1 = TAX_DICT1[taxID]
        bestTax2 = TAX_DICT2[taxID]
        bestTax3 = TAX_DICT_COMPLETE[taxID]
        accept = False
        for hit in hits[1:]:
            # check on second hierarchy of taxonomy
            if TAX_DICT2[hit[1].split("_")[0]] != bestTax2:
                accept = True
                break
        # compile a results list
        # result values:
        # average identity best hit, average identity of other hit, ratio of both,
        # e-value best hit, e-value other hit, difference e-values,
        # taxon 1 best hit, taxon 2 best hit, taxonomic lineage best hit,
        # taxon 1 other hit, taxon 2 other hit, taxonomic lineage other hit
        if (accept):
            # handle entries with hits
            taxID = hit[1].split("_")[0]
            tax1 = TAX_DICT1[taxID]
            tax2 = TAX_DICT2[taxID]
            tax3 = TAX_DICT_COMPLETE[taxID]
            bestident = float(str.replace(bestHit[3], "%", ""))
            ident = float(str.replace(hit[3], "%", ""))
            result = [
                bestHit[0],
                "{0:.2f}".format(bestident * (float(bestHit[4]) / maxalign)),
                "{0:.2f}".format(ident * (float(hit[4]) / maxalign)),
                "{0:.2f}".format((bestident * (float(bestHit[4]) / maxalign)) / (ident * (float(hit[4]) / maxalign))),
                "{0:.2f}".format(float(bestHit[2])),
                "{0:.2f}".format(float(hit[2])),
                "{0:.2f}".format(float(bestHit[2]) - float(hit[2])),
                bestTax1,
                bestTax2,
                bestTax3,
                tax1,
                tax2,
                tax3,
            ]
        else:
            # use default values for unaccepted entries/ entries without hits
            bestident = float(str.replace(bestHit[3], "%", "")) / 100
            result = [
                bestHit[0],
                "{0:.2f}".format(bestident * float(bestHit[4]) / maxalign),
                "NA",
                "{0:.2f}".format(100),
                "{0:.2f}".format(float(bestHit[2])),
                "NA",
                "{0:.2f}".format(-100),
                bestTax1,
                bestTax2,
                bestTax3,
                "NA",
                "NA",
                "NA",
            ]
    return result


def combine_hits(forward, reverse, combine, output):
    """Writes concordantly or best accepted hits for each read pair.

    Note:
        The temporary input files forward and reverse are deleted by this function.

    Arguments:
        forward (str/path): Temporary taxa file written by run() for paired end reads.
        reverse (str/path): Temporary taxa file written by run() for paired end reads.
        combine (str): Combination type, can be 'best' or 'concordant' (Default: 'best'; see Argparser).
        output (str/path): Path to the output taxa.tsv file.
    """
    with open(output, "w") as out:
        with open(forward, 'r') as tmp_forward, open(reverse, 'r') as tmp_reverse:
            for forw_tax, rev_tax in zip(tmp_forward, tmp_reverse):
                forw_tax = forw_tax.strip().split("\t")
                rev_tax = rev_tax.strip().split("\t")
                if (combine == "concordant"):
                    # check if bestTax are the same, then use the one with better e-value, for NA it doesn't matter
                    if (forw_tax[8] == rev_tax[8]):
                        if (float(forw_tax[4]) <= float(rev_tax[4])):
                            out.write("\t".join(forw_tax))
                        else:
                            out.write("\t".join(rev_tax))
                    else:
                        result = [forw_tax[0], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
                        out.write("\t".join(result))
                else:
                    # combine == 'best'
                    if (forw_tax[8] != "NA" and rev_tax[8] != "NA"):
                        if(float(forw_tax[4]) <= float(rev_tax[4])):
                            out.write("\t".join(forw_tax))
                        else:
                            out.write("\t".join(rev_tax))
                    elif (forw_tax[8] != "NA"):
                        out.write("\t".join(forw_tax))
                    elif (rev_tax[8] != "NA"):
                        out.write("\t".join(rev_tax))
                    else:
                        out.write("\t".join(forw_tax))
                out.write("\n")
    remove(forward)
    remove(reverse)


def calculate_mean_identities(output):
    """Compute median identity for both taxonomic levels and write them to an identities.tsv file."""
    sumTax1 = {key: 0 for key in TAX_DICT1.values()}
    sumTax2 = {key: 0 for key in TAX_DICT2.values()}
    counterTax1 = {key: 0 for key in sumTax1}
    counterTax2 = {key: 0 for key in sumTax2}
    outfile2 = path.splitext(output)[0] + "_identities.tsv"
    with open(output, "r") as tmp:
        for line in tmp:
            line = line.rstrip().split("\t")
            ident = line[1]
            if(ident != "NA"):
                tax1 = line[7]
                tax2 = line[8]
                sumTax1[tax1] += float(ident)
                sumTax2[tax2] += float(ident)
                counterTax1[tax1] += float(1)
                counterTax2[tax2] += float(1)
    with open(outfile2, "w") as out2:
        for i in sumTax1.keys():
            mean1 = (sumTax1[i] + 1) / (counterTax1[i] + 1)
            out2.write(i + "\t" + str(mean1) + "\n")
            out2.write("{}\t{}\n".format(i, str(mean1)))
        for i in sumTax2.keys():
            mean2 = (sumTax2[i] + 1) / (counterTax2[i] + 1)
            out2.write("{}\t{}\n".format(i, str(mean2)))
#
#
# def rename_output(tmp, aln, output):
#     """Rename generated outfile in file system to fit output parameter.
#
#     Arguments:
#         tmp (str/path): The output file used by analyse_hits()
#         aln (str/path): Basename of the .aln input file.
#         output (str/path): Output filename as specified in args.output.
#     """
#     alnfile = path.basename(aln)
#     output2 = tmp + "_" + alnfile
#     rename(output2, output)


def main():
    parser = argparse.ArgumentParser()
    # add specialized parameters for mapping to the argparser.
    sub(parser)
    args = parser.parse_args()
    run(parser, args)


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
