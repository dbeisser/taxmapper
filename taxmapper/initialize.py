#!/usr/bin/env python3
"""Submodule of TaxMapper downloading and creating the reference database index.

Call prerapsearch with the downloaded database.
This creates '.db' index file.
"""
import argparse
import os
import urllib.request

from subprocess import call


def sub(parser):
    parser.add_argument(
        '-d', '--database',
        dest='database',
        help='Output path: RAPSearch database index',
        type=str,
        required=True,
    )


def humansize(byte_size):
    """Return human readable size of filesize."""
    suffixes = ["B", "KiB", "MiB", "GiB", "TiB"]
    suffix = 0
    while (byte_size >= 1024) and (suffix < 4):
        suffix += 1
        byte_size = byte_size / 1024
    if suffix == 0:
        return "{} {}".format(byte_size, suffixes[suffix])
    else:
        return "{:.1f} {}".format(byte_size, suffixes[suffix])


def create_index(database):
    """Check if needed database is present.

    If not, query the user if it should be downloaded, download and create index.
    """
    path = os.path.dirname(database)
    databases = [
        (os.path.join(path, "meta_database.fa.gz"), "https://bitbucket.org/dbeisser/taxmapper_supplement/raw/master/databases/taxonomy/meta_database.fa.gz"),
    ]
    for download, url in databases:
        if not os.path.exists(database):
            filesize = int(urllib.request.urlopen(url).info()["Content-length"])
            print("Taxonomic database is missing. Should it be downloaded? ({})".format(humansize(filesize)))
            i = input("[Y]/n ")
            if i in ("", "y", "Y"):
                file_name, _ = urllib.request.urlretrieve(url, download)
                command = ["gunzip", os.path.join(path, "meta_database.fa.gz")]
                call(command)
                print("Starting prerapsearch")
                command = ["prerapsearch", "-d", os.path.join(path, "meta_database.fa"), "-n", database]
                call(command)
            else:
                sys.exit(1)


def run(args):
    """Prepare calls to prerapsearch. Manage input and output files.

    Creates index output file from argument database.
    """
    create_index(args.database)


def main():
    parser = argparse.ArgumentParser()
    sub(parser)
    args = parser.parse_args()
    run(args)


if __name__ == '__main__':
    main()

