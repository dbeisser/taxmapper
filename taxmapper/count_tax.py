#!/usr/bin/env python3
"""counts hits for each taxon from a taxonomy mapping file on two taxonomic levels.
"""
import argparse


def sub(parser):
    parser.add_argument(
        '-i', '--tax',
        dest='taxa',
        type=str,
        help='Filtered taxonomy mapping file.',
        required=True,
    )
    parser.add_argument(
        '--out1',
        dest='output1',
        type=str,
        default="taxa_counts_level1.tsv",
        help='Output file 1, counted taxa for first taxonomic hierarchy [default: taxa_counts_level1.tsv]',
    )
    parser.add_argument(
        '--out2',
        dest='output2',
        type=str,
        default="taxa_counts_level2.tsv",
        help='Output file 2, counted taxa for second taxonomic hierarchy [default: taxa_counts_level2.tsv]',
    )


def run(parser, args):
    tax1_result, tax2_result = count_taxa(args.taxa)
    with open(args.output1, "w") as f:
        for taxon, count in tax1_result.items():
            f.write("{}\t{}\n".format(taxon, count))
    with open(args.output2, "w") as g:
        for taxon, count in tax2_result.items():
            g.write("{}\t{}\n".format(taxon, count))


def count_taxa(input_path):
    """Return a list of accepted hits or NA for each read."""
    taxon1 = {}
    taxon2 = {}
    with open(input_path, 'r') as in_file:
        for line in in_file:
            _, tax1, tax2, *_ = line.rstrip().split("\t")
            
            if (tax1 not in taxon1):
                taxon1[tax1] = 1
            else:
                taxon1[tax1] += 1
            if (tax2 not in taxon2):
                taxon2[tax2] = 1
            else:
                taxon2[tax2] += 1
    return taxon1, taxon2


def main():
    parser = argparse.ArgumentParser()
    sub(parser)
    args = parser.parse_args()
    run(parser, args)


if __name__ == '__main__':
    main()

