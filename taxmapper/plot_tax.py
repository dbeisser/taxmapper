#!/usr/bin/env python3
"""Evaluate a taxa file, save counts and frequencies, and plot the results as svg.
"""

import argparse
import os
import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')  # if DISPLAY variable is not set.  This uses a non-interactive backend to plot to file.
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap


def sub(parser):
    parser.add_argument(
        '-t', '--taxa',
        dest='taxa',
        type=str,
        nargs='+',
        help='Taxonomy file(s), counted taxa for a taxonomic hierarchy',
        required=True,
    )
    parser.add_argument(
        '-s', '--samples',
        dest="samples",
        type=str,
        nargs='+',
        help='Optional sample names, sample names have to be in the same order '
             'as taxonomy input files. If no samplenames are provided, the '
             'base names of the taxa file(s) will be used.',
    )
    parser.add_argument(
        '-f', '--freq',
        dest='freq',
        type=str,
        default="taxa_freq_norm.tsv",
        help='Output file 1, taxon matrix with normalized frequencies [default: taxa_freq_norm.tsv]',
    )
    parser.add_argument(
        '-c', '--counts',
        dest='counts',
        type=str,
        default="taxa_counts.tsv",
        help='Output file 2, taxon matrix with counts [default: taxa_counts.tsv]',
    )
    parser.add_argument(
        '-p', '--plot',
        dest='plot',
        type=str,
        default="taxa_freq_norm.svg",
        help='Output file 3, stacked barplot of total count normalized taxa [default: taxa_freq_norm.svg]',
    )


def run(parser, args):
    """Handle input files. Call normalization and plot results."""
    # if no sample names are given, derive sample names from the taxonomy files.
    if not (args.samples):
        samples = [os.path.splitext(os.path.basename(taxa_file))[0] for taxa_file in args.taxa]
    else:
        samples = args.samples
    result_1, result_2 = normalize(args.taxa, samples)
    # write the results (pandas dataframes) to disc as
    result_1.to_csv(args.counts, sep='\t', na_rep="NA")
    result_2.to_csv(args.freq, sep='\t', na_rep="NA")
    plot_tax(result_2, plotfile=args.plot, size=len(samples)/4)


def normalize(taxa, samples):
    """Calculate normalized read counts, frequencies"""
    tax_dict = dict()
    if (type(samples) is not list):
        samples = [samples]
        taxa = [taxa]
        # WARNING what if taxa already is a list? Counterpoint,
        # In [3]: list(zip(["sample1", "sample2", "sample3"], "basename.tax"))
        # Out[3]: [('sample1', 'b'), ('sample2', 'a'), ('sample3', 's')]
        # Also: What if you have more than 1 sample, but only one
        # In [4]: list(zip(["sample1", "sample2", "sample3"], ["basename.tax"]))
        # Out[4]: [('sample1', 'basename.tax')]
        # This ignores sample names, if more sample names than tax files are given.
        # Example:  `python plot_tax.py -t tax1.tax -s foo, bar`  ignores the bar
        # And ignores taxa files, if more taxa files then sample names are given:
        # python plot_tax.py -t tax1.tax tax2.tax -s foo
        # ['foo'] ['tax1.tax', 'tax2.tax'] -> ignores tax2.tax
    for sample, tax_path in zip(samples, taxa):
        with open(tax_path, 'r') as in_file:
            taxon = []
            counts = []
            for line in in_file:
                line = line.rstrip().split("\t")
                taxon.append(line[0])
                counts.append(int(line[1]))
        tax_dict[sample] = (pd.Series(counts, index=taxon))
    df = pd.DataFrame(tax_dict)
    df2 = df.loc[df.index[df.index != "NA"]]  # remove all NA values
    df3 = df2 / df2.sum(0)  # compute freqeuncy of deteced taxa
    return df2, df3


# def plot_tax(df, plotfile, size):
#     """Plot the taxonomy counts as a barchart.

#     Arguments:
#         df (DataFrame): Containing the data to be plotted.
#         plotfile (str/path): Path to the output file.
#         size (int): Figure hight in inches. (Will be passed to DataFrame.plot.barh(..., figsize=(10, size)))
#     """
#     print("Dataframe", df)

#     # prepare plotting parameters
#     mpl.style.use('ggplot')
#     mpl.style.use('seaborn-whitegrid')
#     # Use paired colors from colormap and interpolate between them
#     colors = [(0.65098039, 0.80784314, 0.89019608, 1), (0.12156863, 0.47058824, 0.70588235, 1), (0.69803922, 0.8745098 , 0.54117647, 1), (0.2 , 0.62745098, 0.17254902, 1), (0.98431373, 0.60392157, 0.6 , 1), (0.89019608, 0.10196078, 0.10980392, 1), (0.99215686, 0.74901961, 0.43529412, 1), (1. , 0.49803922, 0., 1), (0.79215686, 0.69803922, 0.83921569, 1), (0.41568627, 0.23921569, 0.60392157, 1), (1. , 1. , 0.6, 1), (0.69411765, 0.34901961, 0.15686275, 1)]
#     cmap_name = 'my_list'
#     # Create the colormap
#     cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=len(df))
#     my_colors = cm(np.linspace(0, 1, len(df))) # Set2
#     # prepare figure to plot into
#     dft = df.transpose()
#     fig = plt.figure()
#     dft.plot.barh(
#         stacked=True,
#         color=my_colors,
#         width=.9,
#         figsize=(10, size),
#         edgecolor="white",
#     )
#     plt.xlim(0, 1)
#     lgd = plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=1)
#     plt.savefig(plotfile, format='svg', bbox_extra_artists=(lgd,), bbox_inches='tight')


def plot_tax(df, plotfile, size):
    dft = df.transpose()
    mpl.style.use('ggplot')
    mpl.style.use('seaborn-whitegrid')
    fig = plt.figure()
    # Use paired colors from colormap and interpolate between them
    colors = [(0.65098039, 0.80784314, 0.89019608, 1), (0.12156863, 0.47058824, 0.70588235, 1), (0.69803922, 0.8745098 , 0.54117647, 1), (0.2 , 0.62745098, 0.17254902, 1), (0.98431373, 0.60392157, 0.6 , 1), (0.89019608, 0.10196078, 0.10980392, 1), (0.99215686, 0.74901961, 0.43529412, 1), (1. , 0.49803922, 0., 1), (0.79215686, 0.69803922, 0.83921569, 1), (0.41568627, 0.23921569, 0.60392157, 1), (1. , 1. , 0.6, 1), (0.69411765, 0.34901961, 0.15686275, 1)]
    cmap_name = 'my_list'
    # Create the colormap
    cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=len(df))
    my_colors = cm(np.linspace(0, 1, len(df))) # Set2
    dft.plot.barh(stacked=True, width = .9, figsize=(10, size), edgecolor = "white", color=my_colors)
    plt.xlim(0, 1)
    lgd = plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=1)
    plt.savefig(plotfile, format='svg', bbox_extra_artists=(lgd,), bbox_inches='tight')


def main():
    parser = argparse.ArgumentParser()
    sub(parser)
    args = parser.parse_args()
    run(parser, args)


if __name__ == '__main__':
    main()
