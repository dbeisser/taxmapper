#!/usr/bin/env python3
"""Filter a taxa.tsv file returned by `taxmapper map`.
"""
import argparse
from math import isnan, exp

# coefficients used for computation of mapping probability
COEFS = [0.0860251261613432, -4.59000792421101e-05, -1.15317191899399, -0.233018103455544, -1.16241239387293, -4.79899135640137]
# Dict to translate tax file entry to base frequency
BASEFREQ = {"Amoebozoa":0.03890529, "Apicomplexa":0.0126357782, "Apusozoa":0.0063167055, "Bacillariophyta":0.0785098211, "Bigyra":0.0143057609, "Cercozoa":0.0207752043, "Chlorophyta":0.0626148022, "Choanoflagellida":0.012125054, "Chromerida":0.0105917742, "Chrysophyceae":0.0367966641, "Ciliophora":0.064138316, "Cryptophyta":0.0369323286, "Dinophyceae":0.186777111, "Euglenozoa":0.018964349, "Foraminifera":0.0321042521, "Fornicata":0.0053180194, "Fungi":0.0195028057, "Glaucocystophyceae":0.0076821234, "Haptophyta":0.0535354154, "Heterolobosea":0.0101406541, "Metazoa":0.1001072521, "Pseudofungi":0.0241020982, "Opisthokonta Rest":0.0088396226, "Parabasalia":0.0150997498, "Perkinsea":0.0066812911, "Rhodophyta":0.0079576403, "Stramenopile Rest":0.0562690173, "Streptophyta":0.0522710993, "NA":0}

def sub(parser):
    parser.add_argument(
        '-i', '--tax',
        dest='taxa',
        type=str,
        help='Taxonomy mapping file (taxa.tsv if not specified otherwise).',
        required=True,
    )
    parser.add_argument(
        '-o', '--out',
        dest='output',
        type=str,
        default="taxa_filtered.tsv",
        help='Output file [default: taxa_filtered.tsv]',
    )
    parser.add_argument(
        '-a', '--auto',
        dest='automatic',
        type=float,
        default=0.4,
        help='Automatic filter with probability threshold, if automatic filter is chosen all other thresholds will be ignored [nan or 0 - 1, default: 0.4]',
    )
    parser.add_argument(
        '--identity',
        dest='identity',
        type=float,
        default='nan',
        help='Threshold for identity of best hit [default: nan]',
    )
    parser.add_argument(
        '-r', '--identity-ratio',
        dest='ratio',
        type=float,
        default='nan',
        help='Threshold for identity ratio [1 - 10, default: nan]',
    )
    parser.add_argument(
        '--evalue',
        dest='evalue',
        type=float,
        default='nan',
        help='Threshold for log e-values of best hit [default: nan]',
    )
    parser.add_argument(
        '-d', '--evalue-diff',
        dest='diff',
        type=float,
        default='nan',
        help='Threshold for absolute difference in e-values [0 - 100, default: nan]',
    )


def run(parser, args):
    """Validate user parameters and apply filter to taxon file."""
    # check if the given parameters are reasonable
    if (args.automatic != 'nan'):
        if not (float(args.automatic) >= 0 and float(args.automatic) <= 1):
            parser.error("The specified probability threshold ({}) is not realistic, please provide a value between 0 and 1!".format(args.automatic))
    else:
        if not (float(args.ratio) >= 1 and float(args.ratio) <= 10):
            parser.error("The specified identity ratio is not realistic, please provide a value between 1 and 10!")
        if not (float(args.diff) >= 0 and float(args.diff) <= 100):
            parser.error("The specified difference in e-values is not realistic, please provide a value between 1 and 100!")
    # filter lines of the taxon file according to 
    with open(args.output, "w") as out:
        with open(args.taxa, "r") as taxonfile:
            for line in taxonfile:
                result = apply_filter(line, args)
                out.write(result)


def apply_filter(line, args):
    """Return a filtered hit or NA for a read"""
    # split up a line from a tax file, which consists of:
    #
    # average identity best hit, average identity of other hit, ratio of both,
    # e-value best hit, e-value other hit, difference e-values,
    # taxon 1 best hit, taxon 2 best hit, taxon 3 best hit,
    # taxon 1 other hit, taxon 2 other hit, taxon3 other hit
    #
    # for details see map_tax l. 103f
    line = line.rstrip().split("\t")
    tax1best, tax2best = line[7], line[8]
    tax1, tax2 = line[10], line[11]
    # if no assignments, return NAs
    if(tax2best == "NA" and tax2 == "NA"):
        return("{}\t{}\n".format(line[0], "\t".join(["NA", "NA", "NA"])))
    identity = float(line[1])
    ratio = float(line[3])
    evalue = float(line[4])
    diff = float(line[6])
    taxident = float(tax1best == tax1)
    basefreq = float(BASEFREQ[tax2])
    # if only best hit, return this one if it meets e-value and identity thresholds
    if(tax2 == "NA"):
        if((identity >= args.identity or isnan(args.identity)) and (evalue <= args.evalue or isnan(args.evalue))):
            return("{}\t{}\n".format(line[0], "\t".join(line[7:10])))
    # two taxa assigned
    else:
        # for automatic filter
        if(not isnan(args.automatic)):
            prob = mapping_probability(identity, ratio, evalue, diff, basefreq)
            if(prob >= args.automatic):
                return("{}\t{}\n".format(line[0], "\t".join(line[7:10])))
            else:
                return("{}\t{}\n".format(line[0], "\t".join(["NA", "NA", "NA"])))
        else:
            # check if best hit meets e-value and identity thresholds
            if((identity >= args.identity or isnan(args.identity)) and (evalue <= args.evalue or isnan(args.evalue))):
                # check if ratio and difference meet criteria
                if((ratio >= args.ratio or isnan(args.ratio)) and (abs(diff) >= args.diff or abs(diff) == isnan(args.diff))):
                    # if true return best hit
                    return("{}\t{}\n".format(line[0], "\t".join(line[7:10])))
                else:
                    # if criteria are not met, but higher taxonomic assignment is the same return this
                    if(taxident):
                        return("{}\t{}\n".format(line[0], "\t".join([line[7], "NA", "NA"])))
    # in all other cases return NAs
    return("{}\t{}\n".format(line[0], "\t".join(["NA", "NA", "NA"])))


def mapping_probability(identity, ratio, evalue, diff, basefreq):
    """Compute mapping probability from extracted from a taxa tsv line.

    Arguments:
        identity (float): Best identity returned by RAPsearch.
        ratio (float): Difference between best and second best identity.
        evalue (float): Best e-value returned by RAPsearch.
        diff (float): Difference between best and second best e-value.
        basefreq (float): Frequency of base in database.

    Returns:
        float: mapping probability.
    """
    prob = 1 / (1 + exp(-(COEFS[0] + COEFS[1] * identity + COEFS[2] * ratio + COEFS[3] * evalue + COEFS[4] * diff + COEFS[5] * basefreq)))
    return(prob)


# Gather the code in a main() function
def main():
    """Main function used to call filter_tax as a standalone step.
    
    Calling it as part of the pipeline is handled by __init__.py
    """
    parser = argparse.ArgumentParser()
    # create specialized (sub) parser for the 'filter' command.
    sub(parser)
    args = parser.parse_args()
    run(parser, args)


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()

