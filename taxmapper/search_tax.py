#!/usr/bin/env python3
"""Submodule of TaxMapper handling calls to RAPsearch.

Call RAPsearch with a given query on a prepared database.
This creates '.aln' files.
"""
import argparse
import os
import sys
from subprocess import call

# default call for the RAPsearch executable
# can be overridden by the --rapsearch argument
RAPSEARCH = "rapsearch"


def sub(parser):
    parser.add_argument(
        '-f', '--forward',
        dest='file1',
        type=str,
        help='Forward reads in fasta or fastq format',
        required=True,
    )
    parser.add_argument(
        '-r', '--reverse',
        dest='file2',
        type=str,
        help='Reads in fasta or fastq format [optional]',
    )
    parser.add_argument(
        '--rapsearch',
        dest='rapsearch',
        help='Rapsearch path, version >=2.24 [default: rapsearch set in PATH variable]',
    )
    parser.add_argument(
        '-d', '--database',
        dest='database',
        help='Path to RAPSearch database index',
        required=True,
    )
    parser.add_argument(
        '-o', '--out',
        dest='output',
        type=str,
        default="hits",
        help='Basename for output files [default: <input>_hits]',
    )
    parser.add_argument(
        '-t', '--threads',
        dest='threads',
        default="4",
        type=str,
        help='Number of threads [default: 4]',
    )


def run(args):
    """Prepare calls to rapsearch. Manage input and output files.

    Creates output files with '_tmp' suffix which are then cleaned and
    written as the final '.aln' alignment files.
    """
    if not (args.output):
        out = os.path.splitext(os.path.split(args.file1)[1])[0]+"_hits"
    else:
        out = args.output
    if not (args.rapsearch):
        # use the default rapsearch path
        rapsearch = RAPSEARCH
    else:
        rapseach = args.rapsearch
    # create db index
    python = sys.executable
    programPath = os.path.dirname(os.path.realpath(__file__))
    tool = os.path.join(programPath, "initialize.py")
    command = [python, tool, "-d", args.database]
    call(command)
    # call rapsearch on given input file or files
    if (args.file1 and args.file2):
        output = out+"_1"
        run_rapsearch(args.file1, rapsearch, output+"_tmp", args.threads, args.database)
        shorten_output(output+"_tmp.aln", output+".aln")
        output = out+"_2"
        run_rapsearch(args.file2, rapsearch, output+"_tmp", args.threads, args.database)
        shorten_output(output+"_tmp.aln", output+".aln")
    else:
        output = out+"_1"
        run_rapsearch(args.file1, rapsearch, output+"_tmp", args.threads, args.database)
        shorten_output(output+"_tmp.aln", output+".aln")


def run_rapsearch(query, rapsearch, output, threads, database):
    """Call rapsearch to find similarity of the reads with the protein database.

    Arguments:
        query (str/path): Path to a query file in FASTA or FASTQ format.
        rapsearch (str): path to the rapsearch execuatble.
        output (str/path): output prefix relayed to RAPsearch.
        threads (int): Nr of threads for RAPsearch to use.
        database (str/path): path to the RAPsearch databse index.
    """
    print("Starting RAPSearch with "+threads+" threads")
    command = [rapsearch, "-q", query, "-d", database, "-o", output, "-z", str(threads), "-a", "T", "-b", "20", "-v", "0", "-p", "T", "-e", "5"]
    call(command)


def shorten_output(outtmp, output):
    """Scan temporary .aln files (raw output from RAPSearch) and remove unneeded information.
    Remove non-hits, remove alignments, keep only read name and mapping quality.
    This is done to save size in the output files.

    Arguments:
        outtmp (str/path): Temporary alignment file. Raw output of RAPSearch.
        output (str/path): Path for the final .aln file.
    """
    with open(output, "w") as aln_file:
        with open(outtmp, "r") as tmp_file:
            keep = [" vs ", "	NO HIT"]
            for line in tmp_file:
                if any(list(map(lambda x: x in line, keep))):
                    aln_file.write(line)
    os.remove(outtmp)


# Gather the code in a main() function
def main():
    """Main function used to call search_tax as a standalone step.

    Calling it as part of the pipeline is handled by __init__.py
    """
    parser = argparse.ArgumentParser()
    # create specialized (sub) parser for the 'search' command.
    sub(parser)
    args = parser.parse_args()
    run(args)


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
