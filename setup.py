from setuptools import setup

# load version info
exec(open("taxmapper/version.py").read())

setup(name='taxmapper',
      version=__version__,
      description='Functions for mapping and filtering of Illumina sequencing read data using a protist reference database.',
      url='http://bitbucket.org/dbeisser/taxmapper',
      author='Daniela Beisser',
      author_email='daniela.beisser@uni-due.de',
      license='MIT',
      packages=['taxmapper'],
      install_requires=['numpy', 'pandas', 'matplotlib'],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'],
      entry_points = {'console_scripts': ['taxmapper=taxmapper:main'],
    }
)
